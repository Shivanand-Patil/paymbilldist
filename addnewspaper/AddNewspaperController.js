app.controller('addNewspaperController', function($scope, $stateParams, Addnewspaper, toaster, $state) {

    $scope.addnewspaper = new Addnewspaper();
    console.log('in Addnespaper create controller', $scope.addnewspaper);

    $scope.addNewspaper = function() {
        console.log('adding', $scope.addnewspaper);
        $scope.addnewspaper.$save(function(response) {
            console.log("addnewspaper save",response);
            if (response.success) {
                 $state.go('main.addnewspaper'); 
            } else {
                toaster.pop('warning', "already Exist, please try with another");
            }
        }, function(error) {
                      console.log("addnewspaper save",error);

            toaster.pop('error', "Add User ERROR");
        });
    }
});
