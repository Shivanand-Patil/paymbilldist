app.factory('AddNewspaper',function($resource){
    return $resource('api/AddNewspaper/:addnewspaper_id',{addnewspaper_id:'@_addnewspaper_id'},{
        update: {
            method: 'PUT'
        }
    });
});